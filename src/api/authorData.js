module.exports = {
    authors:
    [
        {
            id: 'foo',
            firstName: 'Adam',
            lastName: 'Baranowsky'
        },
        {
            id: 'bar',
            firstName: 'Eli',
            lastName: 'Kowalsky'
        },
        {
            id: 'fab',
            firstName: 'Grajine',
            lastName: 'Ronaldo'
        }
    ]
};