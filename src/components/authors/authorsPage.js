'use strict';

var React = require('react');
var AuthorApi = require('../../api/autorAPI');
var AuthorList = require('./authorList');

var AuthorsPage = React.createClass({
    getInitialState: function(){
        return {
            authors: []
        };
    },
    componentWillMount: function(){
        this.setState({authors: AuthorApi.getAllAuthors()});
    },
    render: function(){
        
        return (
            <div>
                <AuthorList authors={this.state.authors} />
            </div>
        );
    }
});

module.exports = AuthorsPage;